export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR=/usr/bin/vim

if [ $TERM = "st-256color" ]
then
    export TERM="xterm-256color"
fi

PATH="${HOME}/scripts/:$PATH"

[ -r ~/.aliases ] && source ~/.aliases

