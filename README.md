# My dot files

## Setup

### Vim

On first run of vim, run ```:PlugInstall```

### Zsh

Install oh-my-zsh:

* From AUR

  Install *oh-my-zsh-git*

  ```/usr/share/oh-my-zsh/tools/install.sh```

* From git

  ```sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"```

### Xresources theme

Install Xresources themes:

```git clone https://github.com/logico-dev/Xresources-themes.git ~/.config/Xresources-themes```

## Description

Configures

### Aliases

  * ls aliases addition to ll, la, and l:
    * **lrt** ll with newest at the botom
    * **lrtt** last 10 lines of lrt
  * **ip** command has color
  * **psg** performs a ```ps -ef``` and grep
  * **hbr1** radio stations in mplayer:
    * **hbr1_ambient**
    * **hbr1_trance**
    * **hbr1_tronic**
  * **mkcd** make and cd to directory
  * **json** view json files

### Bash

  * history
    * No duplicated lines in history
    * Unlimited file size
  * globstar for ``**`` to represent any number of directories in a search
  * git branch in prompt
  * git completion

### Simple terminal

  * $TERM set to xterm-256color as vim doesn't work properly with st-256color

### Vim

  * Command mode:
    * **Ctrl-a** goes to beginning of line
  * Visual mode:
    * **Ctrl-c** copies to clipboard
  * **Shift-Ins** pasts
  * Folding enabled
  * Strip trailing whitespace from lines in code on buffer write
  * Theme: Dark gruvbox-material

### ZSH

oh-my-zsh with:
  * gruvbox theme
  * Case sensative completion

